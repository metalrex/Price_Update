import requests
from bs4 import BeautifulSoup
from time import sleep
from twilio.rest import Client

def whtsapp_notify_to_update(search_result,account_sid,auth_token):
    account_sid = account_sid 
    auth_token = auth_token 
    client = Client(account_sid, auth_token) 

    message = client.messages.create( 
                                  from_='whatsapp:+14155238886',  
                                  body=f"Your {search_result['item_name']} is {search_result['item_stock']} in cost of {search_result['item_price']} \n >>!! WAITING TO DROP PRICE !!<<",      
                                  to='whatsapp:+918054405938' 
                              ) 

    return message.sid

def whtsapp_notify_to_buy(search_result,account_sid,auth_token):
    account_sid = account_sid 
    auth_token = auth_token 
    client = Client(account_sid, auth_token) 

    message = client.messages.create( 
                                  from_='whatsapp:+14155238886',  
                                  body=f"Your {search_result['item_name']} is {search_result['item_stock']} in cost of {search_result['item_price']} \n >>!! HURRY BOOK NOW !!<<",      
                                  to='whatsapp:+918054405938' 
                              ) 

    return message.sid

def data_dict_return(title,price,stock): 
    item_data = dict();  
    item_data['item_name'] = title
    item_data['item_price'] = price
    item_data['item_stock'] = stock
    return item_data

def item_resource(soup):
    try:
        title = soup.find(id='productTitle').get_text(separator=" ").strip()
    except:
        title = 'N|A'
        
    try:
        price = soup.find(id='priceblock_ourprice').get_text().replace(',','')
        price = re.search("\d+",price).group()
        price = int(price)
    except Exception as e:
        price = int(0)
        
    try:
        soup.select('#availability .a-color-state')[0].get_text().strip()
        stock = 'Out of Stock'
    except:
        # checking if there is "Out of stock" on a second possible position
        try:
            soup.select('#availability .a-color-price')[0].get_text().strip()
            stock = 'Out of Stock'
        except:
            # if there is any error in the previous try statements, it means the product is available
            stock = 'Available'
    
    item_data = data_dict_return(title, price, stock)
    
    return item_data
    
    

def item_page_source(url,HEADERS):
    page = requests.get(url, headers=HEADERS)
    soup = BeautifulSoup(page.content, features="lxml")
    data = item_resource(soup)
    return data


if __name__ == "__main__":
    
    account_sid = 'AC383f3b07f2539dd3ef6a52c5fd2e198c' 
    auth_token = '1f7e8cb1d7f8d772707cd6d67a705029' 
    
    url = "https://www.amazon.in/New-Apple-iPhone-11-64GB/dp/B08L8DV7BX/ref=sr_1_4?dchild=1&keywords=iphone+11&qid=1614962748&sr=8-4"
    HEADERS = ({'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36','Accept-Language': 'en-IN, en;q=0.5'})
    
    try:
        search_result = item_page_source(url,HEADERS)
        excpected_price = int(49999)
        if search_result['item_price'] >= excpected_price:
            search_result = item_page_source(url,HEADERS)
            notify = whtsapp_notify_to_update(search_result,account_sid,auth_token)
            print(notify)
        else:
            notify = whtsapp_notify_to_buy(search_result,account_sid,auth_token)
            print(notify)
            
    except Exception as e:
        print(e)